fastlane documentation
================
# Installation
```
sudo gem install fastlane
```
# Available Actions
## iOS
### ios clean
```
fastlane ios clean
```
Clean local Pods
### ios lint
```
fastlane ios lint
```
Lint private pod with HTSpecs
### ios push
```
fastlane ios push
```
Push private pod with HTSpecs
### ios sonarqube
```
fastlane ios sonarqube
```
Sonar Qube

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [https://fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [GitHub](https://github.com/fastlane/fastlane/tree/master/fastlane).
